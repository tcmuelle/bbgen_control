all:
	clang bbgen.c -m32 -shared -o bbgen.lib
	clang bbgen_test.c -m32 -o bbgen_test bbgen.lib

test: export LD_LIBRARY_PATH=.
test: all
	./bbgen_test
