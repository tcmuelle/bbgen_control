#include <stdio.h>
#include <stdint.h>
#include "bbgen.h"

int main(){
     uint32_t base_var;
     uint32_t *ptr;
     ptr = &base_var;
     BBGEN_SET_VALUE_RAW((uint32_t) ptr, 0,0);
     printf("%#010x (Expected:0x08000003)\n", base_var);
     for (uint32_t i=0; i<=2000; i +=50) {
	  printf("NWELL[%i mV]: Setting=%i\n", i, BBGEN_NWELL_MV_TO_BIT_CONVERSION(i));
     }
     for(int32_t i=-1700; i<=900; i +=50) {
	  printf("PWELL[%i mV]: Setting=%i\n", i, BBGEN_PWELL_MV_TO_BIT_CONVERSION(i));
     }
     printf("NWELL[28 mV]: Setting=%i (Expected:0)\n", BBGEN_NWELL_MV_TO_BIT_CONVERSION(28));
     printf("NWELL[29 mV]: Setting=%i (Expected:1)\n", BBGEN_NWELL_MV_TO_BIT_CONVERSION(29));
     printf("PWELL[28 mV]: Setting=%i (Expected:0)\n", BBGEN_PWELL_MV_TO_BIT_CONVERSION(28));
     printf("PWELL[29 mV]: Setting=%i (Expected:1)\n", BBGEN_PWELL_MV_TO_BIT_CONVERSION(29));
     printf("PWELL[-36 mV]: Setting=%i (Expected:0)\n", BBGEN_PWELL_MV_TO_BIT_CONVERSION(-36));
     printf("PWELL[-37 mV]: Setting=%i (Expected:-1)\n", BBGEN_PWELL_MV_TO_BIT_CONVERSION(-37));
}
