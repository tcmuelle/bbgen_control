#include <stdio.h>
#include <stdint.h>
#include "bbgen.h"

void BBGEN_WRITE(uint32_t base, union BBGEN_COMMAND_t cmd) {
     uint32_t *ptr = (uint32_t*) base;
     *ptr = cmd.command;
     return;
}

// checkme - based on the endianess the order may be reversed
union BBGEN_COMMAND_t BBGEN_COMMAND(uint8_t opcode, uint8_t byte_2, uint8_t byte_1, uint8_t byte_0) {
     union BBGEN_COMMAND_t cmd;
     cmd.bytes[3] = opcode;
     cmd.bytes[2] = byte_2;
     cmd.bytes[1] = byte_1;
     cmd.bytes[0] = byte_0;
     return cmd;
}

// sets pwell and nwell to a specific binary setting (unprotected)
void BBGEN_SET_VALUE_RAW(uint32_t base, uint8_t nwell, int8_t pwell) {
     // update preset 3
     BBGEN_WRITE(base, BBGEN_COMMAND(BBGEN_OP_SET_PRESET, BBGEN_PRESET_3, nwell, (uint8_t) pwell));
     // apply preset
     BBGEN_WRITE(base, BBGEN_COMMAND(BBGEN_OP_SELECT_PRESET, 0, 0, BBGEN_PRESET_3));
     return;
}

// sets pwell and nwell to a specific value in mV (out of range protected)
void BBGEN_SET_VALUE_MV(uint32_t base, uint32_t nwell, int32_t pwell) {
     BBGEN_SET_VALUE_RAW(base, BBGEN_PWELL_MV_TO_BIT_CONVERSION(nwell), BBGEN_PWELL_MV_TO_BIT_CONVERSION(pwell));
     return;
}

//
int8_t BBGEN_PWELL_MV_TO_BIT_CONVERSION(int32_t pwell) {
     int8_t pwell_byte;
     if (pwell >= 0)
     {
	  pwell_byte = (int8_t) ((pwell + BBGEN_PWELL_POS_OFFSET)/BBGEN_PWELL_POS_STEP_SIZE);
	  return (pwell_byte > BBGEN_PWELL_POS_MAX)?BBGEN_PWELL_POS_MAX:pwell_byte;
		    
     } else {
	  pwell_byte = (int8_t) ((pwell - BBGEN_PWELL_NEG_OFFSET)/BBGEN_PWELL_NEG_STEP_SIZE);
	  return (pwell_byte < BBGEN_PWELL_NEG_MIN)?BBGEN_PWELL_NEG_MIN:pwell_byte;
     }
}

uint8_t BBGEN_NWELL_MV_TO_BIT_CONVERSION(uint32_t nwell) {
     uint8_t nwell_byte;
     nwell_byte = (uint8_t) ((nwell + BBGEN_NWELL_OFFSET)/BBGEN_NWELL_STEP_SIZE);    
     return (nwell_byte > BBGEN_NWELL_MAX)?BBGEN_NWELL_MAX:nwell_byte;
}
